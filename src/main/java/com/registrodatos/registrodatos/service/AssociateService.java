package com.registrodatos.registrodatos.service;


import com.registrodatos.registrodatos.model.Asociado;
import com.registrodatos.registrodatos.repository.IAssociateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AssociateService implements IAssociateService {

    @Autowired
    private IAssociateRepository assiciateRepo;

    @Override
    public List<Asociado> findAll() {
        return assiciateRepo.findAll();
    }

    @Override
    public Optional<Asociado> findById(Integer id) {
        return assiciateRepo.findById(id);
    }

    @Override
    public Asociado create(Asociado asociado) {
        return assiciateRepo.save(asociado);
    }

    @Override
    public Asociado update(Asociado asociado) {
        return assiciateRepo.save(asociado);
    }

    @Override
    public void delete(Integer id) {
        assiciateRepo.deleteById(id);
    }
}
