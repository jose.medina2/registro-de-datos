package com.registrodatos.registrodatos.service;

import com.registrodatos.registrodatos.model.Asociado;

import java.util.List;
import java.util.Optional;

public interface IAssociateService {
    List<Asociado> findAll();

    Optional<Asociado> findById(Integer id);

    Asociado create(Asociado asociado);

    Asociado update(Asociado asociado);

    void delete(Integer id);

}
