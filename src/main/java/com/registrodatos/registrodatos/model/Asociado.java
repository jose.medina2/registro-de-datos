package com.registrodatos.registrodatos.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "asociado")
public class Asociado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAsociado;

    @NotNull(message = "Los nombres no pueden ser nulos")
    @NotBlank(message = "El nombre es obligatorio")
    @Size(min = 3, max = 100, message = "El nombre debe ser mayor a 3 caracteres")
        private String nmbres;

    @NotNull(message = "Los apellidos no pueden ser nulos")
    @NotBlank(message = "El apellido es obligatorio")
    @Size(min = 3, max = 100, message = "El apellido debe ser mayor a 3 caracteres")
    private String apellidos;

    @Size(max = 200, message = "La dirección no debe superar los 200 caracteres")
    @Column(name = "direccion", nullable = true, length = 150)
    private String direccion;

    @Size(min = 7, max = 10, message = "El teléfono debe tener al menos 7 digitos y maximo 10 digitos")
    @Column(name = "telefono", nullable = false, length = 10)
    private String telefono;

    @NotNull
    @NotBlank(message = "El correo es obligatorio")
    @Size(min = 10, message = "EL correo debe ser al menos de 10 caracteres")
    @Email(message = "El correo enviado no es un formato válido")
    @Column(name = "correo", nullable = true, length = 150)
    private String correo;

    @Size(min = 3, max = 250, message = "La ocupacion debe ser mayor a 3 caracteres")
    @Column(name = "ocupacion", nullable = false, length = 250)
    private String ocupacion;

    public Integer getIdAsociado() {
        return idAsociado;
    }

    public void setIdAsociado(Integer idAsociado) {
        this.idAsociado = idAsociado;
    }

    public String getNmbres() {
        return nmbres;
    }

    public void setNmbres(String nmbres) {
        this.nmbres = nmbres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }
}
