package com.registrodatos.registrodatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistroDatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistroDatosApplication.class, args);
	}

}
