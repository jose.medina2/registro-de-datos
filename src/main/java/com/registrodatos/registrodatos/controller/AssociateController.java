package com.registrodatos.registrodatos.controller;

import com.registrodatos.registrodatos.model.Asociado;
import com.registrodatos.registrodatos.service.AssociateService;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/asociado")
public class AssociateController {

    @Autowired
    private AssociateService associateService;
    @GetMapping
    public ResponseEntity<List<Asociado>> findAll() {
        return ResponseEntity.ok(associateService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Asociado> findById(
           // @Parameter(description = "Identificador del cliente que desea obtener")
            @PathVariable("id") Integer idAsociado) {
        return associateService.findById(idAsociado)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    @PostMapping
    public ResponseEntity<Asociado> create( @Valid @RequestBody Asociado asociado) {
        return new ResponseEntity<>(associateService.create(asociado), HttpStatus.CREATED);
    }
    @PutMapping
    public ResponseEntity<Asociado> update(@Valid @RequestBody Asociado asociado) {
        return associateService.findById(asociado.getIdAsociado())
                .map(associate -> ResponseEntity.ok(associateService.update(asociado)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Asociado> delete(
            @PathVariable("id") Integer idAssociate) {
        return associateService.findById(idAssociate)
                .map(associate -> {
                    associateService.delete(idAssociate);
                    return ResponseEntity.ok(associate);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
