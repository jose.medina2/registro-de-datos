package com.registrodatos.registrodatos.repository;

import com.registrodatos.registrodatos.model.Asociado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAssociateRepository extends JpaRepository <Asociado, Integer > {
}
